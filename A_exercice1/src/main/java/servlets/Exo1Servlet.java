package servlets;

import dto.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(urlPatterns = "/")
public class Exo1Servlet extends HttpServlet {
    private User user = new User("bob", "bobmdp");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO appeler la jsp de login
        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//TODO récupérer le login et le mdp et les comparer avec la paire stockée statiquement; appeler la jsp bonjour en cas de succès et la jsp de login sinon
        String login=request.getParameter("login");
        String mdp=request.getParameter("mdp");
        if (mdp.isBlank() || login.isBlank()) {
            error_page(request, response);
        }else {
            if(login.equals(user.getLogin()) && mdp.equals(user.getMdp())) {
                request.setAttribute("login", user.getLogin());
                request.getRequestDispatcher("/WEB-INF/bonjour.jsp").forward(request,response);
            }
            error_page(request, response);
        }
    }

    private void error_page(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("login_error", "Informations incorrect");
        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request,response);
    }
}
