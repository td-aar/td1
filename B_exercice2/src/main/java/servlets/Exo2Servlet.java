package servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/")
public class Exo2Servlet extends HttpServlet {
    private Map<Integer, Option> optionMap = new HashMap<>();
    private Map<Integer, Integer> count = new HashMap<>();

    @Override
    public void init() throws ServletException {
        super.init();
        this.optionMap.put(1, new Option("Beau",1));
        this.optionMap.put(2, new Option("Couvert",2));
        this.optionMap.put(3, new Option("Pluie",3));
        this.optionMap.put(4, new Option("Neige",4));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        toJsp(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // on récupère le paramètre "memo" de la requête
        String meteo=request.getParameter("meteo");
        Integer meteoInt = Integer.valueOf(meteo);
        if(!meteo.isBlank()){
            // on stocke sa valeur en session, sous le nom "memosession"
            request.getSession().setAttribute("previous_choice",meteo);

            if(this.count.containsKey(meteoInt)) {
                this.count.put(meteoInt, this.count.get(meteoInt) + 1);
            } else {
                this.count.put(meteoInt, 1);
            }
        }

        toJsp(request,response);
    }

    private void toJsp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("options",optionMap.values());
        request.setAttribute("count", count);

        request.getRequestDispatcher("WEB-INF/meteo.jsp").forward(request,response);
    }

    public class Option{
        private String value;
        private int key;

        public Option(String value, int key) {
            this.value = value;
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public int getKey() {
            return key;
        }
    }
}
