
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>METEO</title>
</head>
<body>

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Météo</th>
        </tr>
    </thead>
    <c:forEach items="${count}" var="coun">
        <tr>
            <c:forEach items="${options}" var="opt">
                <c:if test="${opt.key == coun.key}">
                    <td>${coun.value}</td>
                    <td>${opt.value}</td>
                </c:if>
            </c:forEach>
        </tr>
    </c:forEach>
</table>

<!-- implanter ici le nombre de fois où chaque option de météo a été validée -->
<form method="post">
    <select name="meteo">
        <c:forEach items="${options}" var="opt">
            <c:choose >
                <c:when test="${opt.key == previous_choice}">
                    <option selected value="${opt.key}">${opt.value}</option>
                </c:when>
                <c:otherwise>
                    <option value="${opt.key}">${opt.value}</option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select>
    <button type="submit">Valider</button>
</form>

</body>
</html>
