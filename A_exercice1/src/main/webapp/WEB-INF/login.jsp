<%--
Cette JSP accueillera un form avec les deux champs login et password
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>LOGIN</title>
</head>
<body>
<!-- dans cette JSP on a deux affichages possibles, suivant que "who_is_there" est défini (et de longueur >0) ou pas... -->
    <form method="post">
        <label> login
            <input type="text" name="login">
        </label>
        <br/>
        <label> mdp
            <input type="password" name="mdp">
        </label>
        <br/>
        <button type="submit">Envoyer</button>
    </form>

<c:if test="${not empty login_error}">
    ${login_error}
</c:if>
</body>
</html>
